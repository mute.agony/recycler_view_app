package com.example.android.recv;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NewActivity extends Activity {

    public static final String EXTRA_MESSAGE = "com.example.recv.MESSAGE";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity);

        Intent currentIntent = getIntent();
        if (currentIntent.hasExtra("newBodyText")) {
            String newText = currentIntent.getExtras().getString("newBodyText");
            TextView bodyView = (TextView) findViewById(
                    R.id.pick_action_activity_text_view);
            bodyView.setText(newText);
        }
        if (currentIntent.hasExtra("pos")) {
            final int position = currentIntent.getExtras().getInt("pos");

            Button buttonTwo = (Button) findViewById(R.id.button_submit);
            buttonTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View buttonTwo) {
                    Intent intent = new Intent();
                    EditText editText = (EditText) findViewById(R.id.activity_input);
                    String message = editText.getText().toString();
                    intent.putExtra(EXTRA_MESSAGE, message);
                    intent.putExtra("pos", position);
                    setResult(1, intent);
                    finish();
                }
            });
        }
    }
}
