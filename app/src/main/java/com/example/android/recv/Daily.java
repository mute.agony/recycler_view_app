package com.example.android.recv;

public class Daily {
    private String mHour;
    private String mAction;

    public Daily(String mHour, String mAction) {
        this.mHour = mHour;
        this.mAction = mAction;
    }

    public String getHour() {

        return mHour;
    }
    public String getAction() {
        return mAction;
    }

    public void setAction(String newAction) {
        this.mAction = newAction;
    }
}