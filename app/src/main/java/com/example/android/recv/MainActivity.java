package com.example.android.recv;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {

    static final int INTENT_REQUEST = 1;
    MyRecyclerViewAdapter adapter;
    ArrayList<Daily> dailies = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // data to populate the RecyclerView with
        //ArrayList<String> animalNames = new ArrayList<>();
        for (int i=5; i<22; i++){
            dailies.add(new Daily(Integer.toString(i), " "));
        }

        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.Daily_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRecyclerViewAdapter(this, dailies);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        
    }

    @Override
    public void onItemClick(View view, int position) {

        String clickText = "You clicked " + adapter.getItem(position) + " on row number " + position;
        Intent startIntent = new Intent(MainActivity.this, NewActivity.class);
        startIntent.putExtra("newBodyText", clickText);
        startIntent.putExtra("pos", position);
        startActivityForResult(startIntent, INTENT_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_REQUEST) {
                String message = data.getStringExtra(NewActivity.EXTRA_MESSAGE);
                int position = data.getExtras().getInt("pos");
                int updateIndex = position;
                if (message != null) {
                    Daily daily = dailies.get(updateIndex);
                    daily.setAction(message);
                    dailies.set(updateIndex, daily);
                }
                adapter.notifyItemChanged(updateIndex);
        }
    }

}